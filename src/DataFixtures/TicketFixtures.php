<?php

namespace App\DataFixtures;

use App\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TicketFixtures extends Fixture implements DependentFixtureInterface
{
    
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 7; $i++) {
            $ticket = new Ticket();
            $ticket->setTitle("Titre {$i}");
            $ticket->setDescription("Description {$i}");
            $ticket->setDate(new \DateTime());
            $ticket->setComment("Commentaire {$i}");
            $ticket->setStatus("Status {$i}");
            $ticket->setSoftware($this->randomizeSoftware());
            $ticket->setAssignedUser(
                $this->getReference("dev@dev.fr")
            );

            $manager->persist($ticket);
            $this->addReference("Ticket_{$i}", $ticket);
        }


        $manager->flush();
    }

    private function randomizeSoftware(){
        $software = "";
        $random = rand(0, 1);
        return $software = ($random) ?  "ITDoc" : "EasyCat";
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

    // Function which looks at $this->getReference("dev@dev.fr") for exemple
    // and assigns a software depending the user's role
}
