<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$email, $password, $roles]) {
            $user = new User();
            $user->setEmail($email);
            $user->setpassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setRoles($roles);

            $manager->persist($user);
            $this->addReference($email, $user);
        }
        $manager->flush();
    }

    private function getUserData(): array{
        return [
            // $userData = [$email, $password, $roles];
            ['user@user.fr', 'user', []],
            ['dev@dev.fr', 'dev', ['ROLE_DEV']],
            ['graphiste@graphiste.fr', 'graphiste', ['ROLE_GRAPHISTE']],
            ['admin_dev@admin_dev.fr', 'admin_dev', ['ROLE_ADMIN_DEV']],
            ['admin_graphiste@admin_graphiste.fr', 'admin_graphiste', ['ROLE_ADMIN_GRAPHISTE']],
            ['super_admin@super_admin.fr', 'super_admin', ['ROLE_SUPER_ADMIN']],
            ['test@test.fr', 'test', ['ROLE_SUPER_ADMIN']]
        ];
    }
}
