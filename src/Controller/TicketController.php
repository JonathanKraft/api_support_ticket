<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\TicketRepository;
use App\Repository\UserRepository;
use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/api/ticket", name="api_ticket")
 */
class TicketController extends Controller
{

    /**
     * @Route("/{id}", name="one_ticket", methods="GET")
     */
    public function findById(Ticket $ticket)
    {
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString(
            $serializer->serialize($ticket, 'json')
        );
    }

    /**
     * @Route("", name="all_tickets", methods={"GET"})
     */
    public function findAll()
    {
        $tickets = $this->getDoctrine()
            ->getRepository(Ticket::class)
            ->findAll();

        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($tickets, 'json'));
    }

    /**
     * @Route("/user/coucou", name="user_ticket", methods="GET")
     */
    public function findTicketsByUser() // Possibility: Admin chooses who's tickets he want to see
    {
        $user = $this->getUser();
        $tickets = $user->getTickets();
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString(
            $serializer->serialize($tickets, 'json')
        );
    }

    //  public function all(UserRepository $repo)
    // {   $user = $this->getUser();
    //     $list = $repo->getBudgetsByUser($user);
    //     $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'sum', 'operations']]);

    //     $response = new Response($this->serializer->serialize($data, 'json'));
    //     return $response;
    // }


    /**
     * @Route("", name="new_ticket", methods={"POST"})
     */
    public function addTicket(Request $req)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $ticket = $serializer->deserialize(
            $req->getContent(),
            Ticket::class,
            "json"
        );
        $manager->persist($ticket);
        $manager->flush();

        $json = $serializer->serialize($ticket, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/{id}", name="update_ticket", methods={"PUT"})
     */
    public function update(Request $request, Ticket $ticket)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();

        $ticket = $serializer->deserialize(
            $req->getContent(),
            Ticket::class,
            "json"
        );
        $ticket->setTitle($update->getTitle());
        $ticket->setDescription($update->getDescription());
        $ticket->setDate($update->getDate());
        $ticket->setComment($update->getComment());
        $ticket->setStatus($update->getStatus());
        $ticket->setSoftware($update->getSoftware());
        $ticket->setAssignedUser($update->getAssignedUser());


        $manager->persist($ticket);
        $manager->flush();

        $json = $serializer->serialize($ticket, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/{id}", name="delete_ticket", methods={"DELETE"})
     */
    public function delete(Ticket $ticket)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($ticket);
        $manager->flush();

        return new Response("OK", 204);
    }

}