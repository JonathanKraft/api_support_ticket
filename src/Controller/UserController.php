<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/api/user", name="api_user")
 */
class UserController extends Controller
{

    /**
     * @Route("/{id}", name="one_user", methods="GET")
     */
    public function findById(User $user) : Response
    {
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString(
            $serializer->serialize($user, 'json')
        );
    }

    /**
     * @Route("", name="all_users", methods={"GET"})
     */
    public function findAll()
    {
        $users = $this->getDoctrine()
        ->getRepository(User::class)
        ->findAll();
        $serializer = $this->get('jms_serializer');
        return JsonResponse::fromJsonString($serializer->serialize($users, 'json'));
    }

    /**
     * @Route("", name="new_user", methods={"POST"})
     */
    public function addUser(Request $req, UserPasswordEncoderInterface $encoder)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();
        $user=$serializer->deserialize($req->getContent(),
                                        User::class,"json");
        $user->setPassword($encoder->encodePassword($user,$user->getPassword()));
        $manager->persist($user);
        $manager->flush();

        $json=$serializer->serialize($user,"json");

        return JsonResponse::fromJsonString($json,201);
    }

    /**
     * @Route("/{id}", name="update_user", methods={"PUT"})
     */
    public function update(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $serializer = $this->get('jms_serializer');
        $manager = $this->getDoctrine()->getManager();

        $user=$serializer->deserialize($req->getContent(),
                                        User::class,"json");
        $user->setEmail($update->getEmail());
        $user->setPassword($encoder->encodePassword($user, $update->getPassword()));
        $user->setRoles($update->getRoles());

        $manager->persist($user);
        $manager->flush();

        $json=$serializer->serialize($user,"json");

        return JsonResponse::fromJsonString($json,201);
    }

    /**
     * @Route("/{id}", name="delete_user", methods={"DELETE"})
     */
    public function delete(User $user)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($user);
        $manager->flush();

        return new Response("OK", 204);
    }

}